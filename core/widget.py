class Widget:
    """Class representing widgets

    These are the widgets visible on top of the dashboard
    """

    def __init__(self, name, html):
        self.name = name
        self.html = html
