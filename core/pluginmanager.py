# -*- coding: utf-8 -*-
from importlib import import_module, reload
import os
import logging
from singleton_decorator import singleton
from core.internalbaseplugin import InternalBasePlugin


@singleton
class PluginManager:
    """ Class managing plugins

    A singleton class made to manage all the plugins from the app
    """

    def __init__(self, locations):
        self.locations = locations
        self.plugins = {}

    def list_plugins(self):
        """
        Get the list of plugins
        :resturn: the plugin list
        """
        plist = []
        for location in self.locations:
            for f in os.listdir(location):
                if os.path.isdir("{0}/{1}".format(location, f)):
                    plist.append(location.replace('../', '') + "." + f)
        return plist

    # name: est préfixé par `plugins.` ou `internal_plugins.`
    def load_plugin(self, name, force_reload=False):
        """
        Import a new plugin
        :param name: name of the plugin
        :param force_reload: Boolean to reload module
        """
        logging.debug("Load plugin " + name)

        if force_reload:
            self.plugins[name] = reload(import_module(name + '.main')).Plugin()
        else:
            self.plugins[name] = import_module(name + '.main').Plugin()

    def unload_plugin(self, name):
        """
        Unload a plugin
        :param list: name of the plugin
        """
        logging.debug("Unload plugin " + name)
        self.plugins[name].__del__()
        self.plugins.pop(name)

    def get_menuItems(self):
        """
        Get all menu items from all plugins
        :return: list of items
        """
        return [val.menu_items for k, val in self.plugins]

    def get_widgets(self):
        """
        Get all widgets from all plugins
        :return: list of widgets
        """
        return [val.widgets for k, val in self.plugins]

    def load_plugins(self):
        """ Load all plugins """
        for m in self.list_plugins():
            self.load_plugin(m)

    def get_plugin(self, name):
        """
        Get a plugin
        :param name: name of the plugin
        :return: the plugin
        """
        return self.plugins[name]

    def get_internal_plugins(self):
        plist = []
        for plugin in self.plugins:
            if plugin.split('.')[0] == "internal_plugins":
                if isinstance(self.plugins[plugin], InternalBasePlugin):
                    plist.append(self.plugins[plugin])
        return plist

    def has_plugin(self, name):
        return "plugins." + name in self.plugins.keys()
