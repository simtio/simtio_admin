import os
import json
from core.config import Config
import logging


class Strings:
    """ Class managing application texts

    A class to load and manage traduction files.
    """

    DEFAULT_LOCALE = 'en'

    def __init__(self, context):
        self.strings = {}
        self.context = context
        self.load_available(context)
        self.load_available('/')

    def load(self, locale, file):
        """
        Load a traduction file
        :param locale: TODO
        :param file: the path of the file to load
        """
        logging.debug("Load locale {0} in context {1}".format(locale, self.context.__class__.__module__))
        if locale not in self.strings:
            self.strings[locale] = {}
        with open(file, encoding='utf-8') as f:
            try:
                self.strings[locale].update(json.load(f))
            except json.decoder.JSONDecodeError as e:
                raise Exception("Error decoding {0}. {1}\n".format(file, e))

    def load_available(self, context):
        """
        Load a traduction file
        :param context: TODO
        """
        av = self.list_available(context)
        for l in av:
            self.load(l['locale'], l['file'])

    def list_available(self, context):
        if context == "/":
            location = 'locales/'
        else:
            m, c, C = context.__class__.__module__.split('.')
            location = m + "/" + c + "/locales/"
        result = []
        for f in os.listdir(location):
            result.append({"locale": f.split('.')[0], "file": location + f})
        return result

    def get_in_all_lang(self, item):
        r = []
        for k in self.strings:
            if item in self.strings[k]:
                v = self.strings[k][item]
            else:
                v = None
            r.append({'locale': k, 'value': v})
        return r

    def __getattr__(self, item):
        locale = Config().get("locale", self.DEFAULT_LOCALE)
        if item in self.strings[locale]:
            return self.strings[locale][item]
        elif item in self.strings[self.DEFAULT_LOCALE]:
            return self.strings[self.DEFAULT_LOCALE][item]
        else:
            return None
