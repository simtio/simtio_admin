import bottle


class Session:
    @staticmethod
    def get(key):
        """
        Get a value from the session
        :param key: Key name
        :return:  Value
        """
        s = bottle.request.environ.get('beaker.session')
        if key in s:
            return bottle.request.environ.get('beaker.session')[key]
        else:
            return None

    @staticmethod
    def set(key, value):
        """
        Set a value in the session
        :param key: Key name
        :param value: Value
        """
        s = bottle.request.environ.get('beaker.session')
        s[key] = value
        s.save()

    @staticmethod
    def remove(key):
        """
        Delete a Key in the session
        :param key: Key name
        """
        s = bottle.request.environ.get('beaker.session')
        del s[key]
        s.save()
