from core.helpers.session import Session


class Flash:
    TYPE_SUCCESS = "is-success"
    TYPE_WARNING = "is-warning"
    TYPE_ERROR = "is-danger"
    TYPE_INFO = "is-info"

    @staticmethod
    def set(type, content):
        Session.set("flash", {
            "type": type,
            "content": content
        })

    @staticmethod
    def has():
        return Session.get("flash") is not None

    @staticmethod
    def read():
        flash = Session.get("flash")
        Session.remove("flash")
        return flash
