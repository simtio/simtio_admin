from singleton_decorator import singleton
import logging


@singleton
class MenuItemManager:
    """ Class managing menu items

    A singleton class made to manage all the items from all plugins
    """

    def __init__(self):
        self.item_list = []

    def append_menuItem(self, item):
        """
        Add a menu item to the app
        :param item: item to be added
        """
        if item in self.item_list:
            return
        logging.debug("Append menu item " + item.text)
        self.item_list.append(item)

    def remove_menuItem(self, item):
        """
        Remove a menu item from the app
        :param name: name of the item
        """
        logging.debug("Remove menu item " + item.text)
        if item in self.item_list:
            self.item_list.remove(item)

    def set_menuItem_list(self, list):
        """
        Replace the item list
        :param list: the new item list
        """
        self.item_list = list

    def get_menuItem_list(self):
        """
        Get the list of items
        :return: item list
        """
        return self.item_list
