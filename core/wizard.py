class Wizard:
    TYPE_STRING = "string"
    TYPE_BOOL = "bool"
    TYPE_NUMBER = "number"
    TYPE_PASSWORD = "password"

    def __init__(self, config_key, question, response_type, parent=None, parent_response=None, default=None):
        self.config_key = config_key
        self.question = question
        self.response_type = response_type
        self.parent = parent
        self.parent_response = parent_response
        if self.response_type == self.TYPE_BOOL and default is None:
            self.default = False
        else:
            self.default = default
