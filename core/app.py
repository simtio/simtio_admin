from core.pluginmanager import PluginManager
from bottle import Bottle
from bottle import static_file
import bottle
from core.config import Config
from beaker.middleware import SessionMiddleware
from core.helpers.session import Session
from core.renderer import Renderer
from core.strings import Strings
from singleton_decorator import singleton
import os
import logging
from core.docker_manager import DockerManager
from ldap3 import Connection
from core.actionlogger import ActionLogger
import json


@singleton
class App:
    """
    Main class, starting the app and managing resources
    """

    def __init__(self, plugin_locations, data_location, address, port, worker_ip, docker_host):
        self.address = address
        self.port = port
        self.pluginManager = PluginManager(plugin_locations)
        Config(data_location + '/config.json')
        ActionLogger(data_location + '/action_history.db')
        logging.getLogger().setLevel(getattr(logging, Config().get("log_level", "INFO")))
        Config().set('worker_ip', worker_ip)
        self.web = Bottle()
        session_opts = {
            'session.type': 'file',
            'session.data_dir': data_location,
            'session.auto': True,
            'save_accessed_time': True,
            'timeout': 1800
        }
        self.app = SessionMiddleware(self.web, session_opts)

        self.web.install(self.setup_wrapper)
        self.web.install(self.auth_wrapper)

        self.strings = Strings('/')

        self.data_location = data_location
        Config().set('data_location', os.getcwd() + "/" + data_location)

        DockerManager(docker_host)

        with open("package.json") as f:
            Config().set('version', json.load(f)['version'])

    @staticmethod
    def setup_wrapper(func):
        """
        Launch setup if not already done
        :param func: TODO
        """

        def wrapper(*args, **kwargs):
            if (not Config().get('locale') or not Config().get('admin_password') or not Config().get('installed')) \
                    and not bottle.request.fullpath.startswith('/internal_plugins/setup/') \
                    and not bottle.request.fullpath.startswith('/assets/'):
                bottle.redirect('/internal_plugins/setup/lang')
            else:
                return func(*args, **kwargs)

        return wrapper

    @staticmethod
    def auth_wrapper(func):
        """
        Ask for authentication if not authentified
        :param func: TODO
        """

        def wrapper(*args, **kwargs):
            if bottle.request.fullpath == "/internal_plugins/authentication/login" \
                    or bottle.request.fullpath.startswith('/assets/') \
                    or bottle.request.fullpath.startswith('/internal_plugins/setup/'):
                return func(*args, **kwargs)

            if Session.get("user") is not None:
                if bottle.request.fullpath.startswith('/internal_plugins/authentication/logout'):
                    return func(*args, **kwargs)

                if not has_ldap() or Session.get('user').name == "admin":
                    return func(*args, **kwargs)
                else:
                    user = Session.get("user")
                    try:
                        user.set_groups(get_groups(user))
                    except Exception:
                        Session().remove("user")
                        bottle.redirect('/internal_plugins/authentication/login')
                    Session.set("user", user)
                    if bottle.request.fullpath.startswith('/internal_plugins/user_dashboard'):
                        return func(*args, **kwargs)
                    else:
                        if Config().get('ldap')['admin_group'] in user.groups:
                            return func(*args, **kwargs)
                        else:
                            bottle.redirect('/internal_plugins/user_dashboard/')

            bottle.redirect('/internal_plugins/authentication/login')

        def has_ldap():
            if "admin_group" in Config().get("ldap", {}).keys():
                if Config().get("ldap", {})["admin_group"] != "":
                    return True
                else:
                    return False

        def get_groups(user):
            ugroups = []
            ldap_conf = Config().get('ldap')
            ldap = Connection(ldap_conf['server'], ldap_conf['user'], password=ldap_conf['password'])
            ldap.bind()
            ldap.search(ldap_conf['admin_group'].split(',', 1)[1], '(objectclass=groupOfNames)',
                        attributes=['cn', 'member'])

            for entry in ldap.response:
                if ldap_conf['search'].format(user.name) in entry['attributes']['member']:
                    ugroups.append(entry['dn'])

            ldap.unbind()
            return ugroups

        return wrapper

    def error_handler(self, error):
        """
        Handle error into error page
        :param error: triggered error
        """
        r = Renderer({'strings': self.strings})
        return r.render('templates/errors.html', {'code': error.status_code})

    def init(self):
        self.pluginManager.load_plugins()
        logging.debug("Create root route")
        self.web.route('/', 'GET', self.root_server)
        logging.debug("Create assets route")
        self.web.route('/assets/<filepath:path>', 'GET', self.assets_server)
        logging.debug("Create 404 route")
        self.web.error(404)(self.error_handler)
        logging.debug("Create 500 route")
        self.web.error(500)(self.error_handler)

        for name, plugin in self.pluginManager.plugins.items():
            for page in plugin.pages:
                m, c = name.split('.')
                logging.debug("Create {0} route for plugin {1}/{2}".format(page.url, m, c))
                self.web.route(
                    '/{0}/{1}/{2}'.format(m, c, page.url),
                    page.method,
                    page.action
                )

    def add_plugin(self, name, force_reload=False):
        """
        Add and load a plugin
        :param name: name of the plugin
        :param force_reload: Boolean to reload module
        """
        logging.debug("Add plugin " + name)
        self.pluginManager.load_plugin(name, force_reload)
        self.init_plugin_pages(name)

    def remove_plugin(self, name):
        """
        Remove and unload a plugin
        :param name: name of the plugin
        """
        logging.debug("Remove plugin " + name)
        self.remove_plugin_pages(name)
        self.pluginManager.unload_plugin(name)

    def init_plugin_pages(self, name):
        """
        Add all pages from a plugin into routes
        :param name: name of the plugin
        """
        plugin = self.pluginManager.get_plugin(name)
        for page in plugin.pages:
            m, c = name.split('.')
            logging.debug("Create {0} route for plugin {1}/{2}".format(page.url, m, c))
            self.web.route(
                '/{0}/{1}/{2}'.format(m, c, page.url),
                page.method,
                page.action
            )

    def remove_plugin_pages(self, name):
        """
        Remove all plugin's pages from routes
        :param name: name of the plugin
        """
        plugin = self.pluginManager.get_plugin(name)
        m, c = name.split('.')
        for page in plugin.pages:
            logging.debug("Remove {0} route for plugin {1}/{2}".format(page.url, m, c))
            self.web.route(
                '/{0}/{1}/{2}'.format(m, c, page.url),
                None, None
            )

    def run(self):
        """ start Bottle server """
        bottle.run(host=self.address, port=self.port, app=self.app)

    def assets_server(self, filepath):
        """ Add main assets """
        return static_file(filepath, root='./assets')

    def root_server(self):
        """ Set the main page to the dashboard """
        bottle.redirect('/internal_plugins/dashboard/')

    def test(self):
        for name, plugin in self.pluginManager.plugins.items():
            for widget in plugin.widgets:
                print('[{0}] {1} : {2}'.format(name, widget.name, widget.html))

            for menuItem in plugin.menu_items:
                print('[{0}] {1} : {2} -> {3}'.format(
                    name,
                    menuItem.text,
                    menuItem.icon,
                    menuItem.link
                ))

            for page in plugin.pages:
                m, c = name.split('.')
                self.web.route(
                    '/{0}/{1}/{2}'
                        .format(m, c, page.url),
                    page.method,
                    page.action
                )

                print(page)

            print(self.web.routes)
