from singleton_decorator import singleton
import logging


@singleton
class WidgetManager:
    """ Class managing widgets

    A singleton class made to manage all the widgets from all plugins
    """

    def __init__(self):
        self.widget_list = []

    def append_widget(self, widget):
        """
        Add a widget to the app
        :param widget: widget to be added
        """
        logging.debug("Append widget " + widget.name)
        self.widget_list.append(widget)

    def remove_widget(self, widget):
        """
        Remove a widget from the app
        :param name: name of the widget
        """
        logging.debug("Remove widget " + widget.name)
        if widget in self.widget_list:
            self.widget_list.remove(widget)

    def set_widget_list(self, list):
        """
        Replace the widget list
        :param list: the new widget list
        """
        self.widget_list = list

    def get_widget_list(self):
        """
        Get the list of widgets
        :return: widget list
        """
        return self.widget_list

    def get_widget(self, name):
        """
        Get a particular widgets
        :return: the widget object
        """
        search_results = [w for w in self.widget_list if w.name == name]
        return search_results[0] if len(search_results) > 0 else None
