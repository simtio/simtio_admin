from singleton_decorator import singleton
from tinydb import TinyDB
from datetime import datetime
from core.helpers.session import Session
import math


@singleton
class ActionLogger:
    MESSAGE_TYPE_INFO = 10
    MESSAGE_TYPE_WARNING = 20
    MESSAGE_TYPE_DANGER = 30

    class ActionLogMessage:
        def __init__(self, user, message, message_type=10):
            self.user = user
            self.message = message
            self.message_type = message_type
            now = datetime.now()
            self.date = now.strftime("%d/%m/%Y")
            self.hour = now.strftime("%H:%M:%S")

        def to_json(self):
            return self.__dict__

    def __init__(self, file=None):
        self.db = TinyDB(file)

    def info(self, message, user=None):
        if user is None:
            user = Session().get("user").name
        self.db.insert(self.ActionLogMessage(user, message, self.MESSAGE_TYPE_INFO).to_json())

    def get_all(self, page):
        limit = 50
        page = page * limit
        return self.db.all()[page:page + limit]

    def get_nb_logs(self):
        return len(self.db)

    def get_nb_pages(self):
        return math.ceil(float(self.get_nb_logs()) / float(50))
