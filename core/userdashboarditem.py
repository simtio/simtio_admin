from core.config import Config


class UserDashboardItem:
    def __init__(self, name, image, url, access=None):
        self.name = name
        self.image = image
        self.url = url
        self.access = access

    def get_access_list(self):
        if self.access is not None:
            access = self.access()
        else:
            access = []
        ldap = Config().get("ldap", None)
        if ldap is not None:
            if ldap["admin_group"] not in access:
                access.append(ldap['admin_group'])
        return access
