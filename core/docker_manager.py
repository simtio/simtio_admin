import subprocess
from singleton_decorator import singleton
import sys
import logging
import docker
from time import sleep


@singleton
class DockerManager:
    """
    Manage docker services
    """
    client = None
    nodes = {}
    services = {}
    containers = {}

    def __init__(self, host=None):
        if host is None:
            self.host = 'tcp://localhost:2375' if sys.platform == 'win32' else 'unix:///var/run/docker.sock'
        else:
            self.host = host
        self.client = docker.DockerClient(base_url=self.host)

        self.reload_monitor()

        # th = threading.Thread(target=self.thread_service_data)
        # th.start()

    def reload_monitor(self):
        actual = self.client.containers.list()
        monitored = [c for c in self.containers.keys()]

        # unbind deleted containers
        for c in monitored:
            if c not in actual:
                del(self.containers[c])

        # bind added containers
        for c in actual:
            if c.name not in self.containers.keys():
                self.containers[c.name] = c.stats(stream=True, decode=True)

    def deploy_stack(self, file, name):
        """
        Deploy docker stack on the machine
        :param file: path of the docker-compose file
        :param name: name of the stack
        """
        logging.debug("Deploy docker stack " + name)
        out = subprocess.run('docker -H {0} stack deploy --with-registry-auth --compose-file {1} {2}'
                             .format(self.host, file, name),
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        print(out)
        if out.returncode == 0:
            r = True
        else:
            r = False
        return r

    def remove_stack(self, name):
        """
        Remove docker stack from the machine
        :param name: name of the stack
        """
        logging.debug("Remove docker stack " + name)
        out = subprocess.run('docker -H {0} stack rm {1}'.format(self.host, name),
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        if out.returncode == 0:
            r = True
        else:
            r = False
        return r

    def list_stacks(self):
        """
        List deployed stacks
        :return: list of stacks
        """
        return self.client.services.list()

    def get_logs(self, name):
        n = name.split('.')[0]
        out = subprocess.run('docker -H {0} service logs {1}'.format(self.host, n),
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        if out.returncode == 0:
            return out.stdout
        return out.stderr

    def has_stack(self, name):
        stacks = self.list_stacks()
        for s in stacks:
            if s.name == name:
                return True
        return False

    def update_dict(self, old_dict, new_dict):
        key = list(new_dict.keys())[0]

        if key in list(old_dict.keys()):
            for k, v in new_dict[key].items():
                if k != 'disk' and k != 'id' and k != 'total_mem':
                    old_dict[key][k] += v
        else:
            old_dict.update(new_dict)

    def get_service_state(self, name):
        out = subprocess.run('docker -H {0} service ls -f "name={1}" --format "{2}"'.format(self.host, name, '{{.Replicas}}'),
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        if out.returncode != 0:
            return 'unknown'

        res = (out.stderr + out.stdout).decode("utf-8").rstrip().split('/')
        if res[0] != res[1]:
            return 'working'
        elif res[0] == '1':
            return 'started'
        elif res[0] == '0':
            return 'stopped'
        else:
            return 'unknown'

    def thread_service_data(self):
        self.reload_monitor()

        stack = DockerManager().list_stacks()

        nodes = {}
        services = {}

        for service in stack:
            tasks = service.tasks()
            cpu, mem, totalMem = 0, 0, 0

            # Service
            status = DockerManager().get_service_state(service.name)

            # Cas d'un service à l'arrêt
            if status == 'stopped':
                self.update_dict(services, {service.name: {"mem": 0, "total_mem": 100, "cpu": 0, "container": service.name, "status": status}})
                continue

            # Recupération de la tâche courrante
            ordered_tasks = [x for x in sorted(tasks, key=lambda d: d['CreatedAt'])]
            task = ordered_tasks[-1]

            # Node
            nodeId = task['NodeID']
            nodeIp = self.client.nodes.get(nodeId).attrs['ManagerStatus']['Addr'].split(':')[0]
            totalMem = self.client.nodes.get(nodeId).attrs['Description']['Resources']['MemoryBytes'] / 1000000

            # Container
            container_name = "{}.{}.{}".format(service.name, task['Slot'], task['ID'])
            if container_name not in self.containers.keys():
                cpu = mem = 0
            else:
                container_stat = next(self.containers[container_name])
                cpu = container_stat['cpu_stats']['cpu_usage']['total_usage'] / container_stat['cpu_stats']['system_cpu_usage'] * 100
                mem = container_stat['memory_stats']['usage'] / 1000000

            # Objects update
            self.update_dict(nodes, {nodeIp: {"mem": mem, "total_mem": totalMem, "cpu": cpu, 'id': nodeId}})
            self.update_dict(services, {service.name: {"mem": mem, "total_mem": totalMem, "cpu": cpu, "status": status}})

        self.nodes = nodes
        self.services = services

    def deploy_temp(self, name, options):
        """
        TODO
        :param name: TODO
        :param options: TODO
        """
        logging.debug("deploy temporary docker " + name)
        out = subprocess.run('docker -H {0} run --rm {1} {2}'.format(self.host, options, name),
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        logging.debug(out)
        return out

    def create_secret(self, name, data, labels=None):
        if labels is None:
            labels = {}
        return self.client.secrets.create(name=name, data=data, labels={})

    def get_secret_by_name(self, name):
        secrets = self.client.secrets.list(filters={"name": name})
        if len(secrets) > 0:
            return secrets[0]
        else:
            return None

    def get_secret_by_id(self, id):
        return self.client.secrets.get(id)

    def delete_secret(self, name):
        try:
            secret = self.get_secret_by_name(name)
        except Exception:
            secret = None
        if secret:
            return secret.remove()
        else:
            return None

    def update_secret(self, name, data, labels=None):
        secret = self.get_secret_by_name(name)
        if secret:
            secret.remove()
            return self.create_secret(name, data, labels)
        else:
            return None

    def has_secret(self, name):
        if self.get_secret_by_name(name):
            return True
        else:
            return False

    def set_secret(self, name, data, labels=None):
        if self.has_secret(name):
            self.update_secret(name, data, labels)
        else:
            self.create_secret(name, data, labels)

    def exec_cmd(self, service, cmd):
        out = subprocess.run('docker -H {0} exec $(docker ps --filter "name={0}" -q) {1}'
                             .format(self.host, service, cmd),
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        return out.stderr + out.stdout

    def node_disk_info(self, node_ip):
        """
        """
        out = subprocess.run("docker -H "+self.host+" system df", stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        while out.returncode != 0:
            out = subprocess.run("docker -H "+self.host+" system df", stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
            sleep(200)

        line = str(out.stdout).split('\\n')[2]
        a = line.split('  ')[-2]
        return a

    def start_service(self, service):
        res = self.client.services.get(service).scale(1)
        return res

    def stop_service(self, service):
        res = self.client.services.get(service).scale(0)
        return res
