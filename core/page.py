class Page:
    """ Class representing the pages of the application """

    METHOD_GET = "GET"
    METHOD_POST = "POST"
    METHOD_PUT = "PUT"
    METHOD_DELETE = "DELETE"

    def __init__(self, url, method, action):
        self.url = url
        self.method = method
        self.action = action
