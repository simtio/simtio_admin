from singleton_decorator import singleton
import logging
from core.config import Config
from ldap3 import Connection


@singleton
class UserDashboardManager:
    def __init__(self):
        self.item_list = []

    def append_item(self, item):
        logging.debug("Append user dashboard item " + item.name)
        self.item_list.append(item)

    def get_list(self):
        return self.item_list

    def get_allowed(self, user):
        ldap = False
        if "admin_group" in Config().get("ldap", {}).keys():
            if Config().get("ldap", {})["admin_group"] != "":
                ldap_conf = Config().get('ldap')

                ldap = Connection(ldap_conf['server'], ldap_conf['user'], password=ldap_conf['password'])

        if not ldap or (not user.ldap and user.name == "admin"):
            return self.get_list()
        else:
            allowed = []
            for item in self.get_list():
                for allowed_group in item.get_access_list():
                    ldap.bind()
                    ldap.search(allowed_group, '(objectclass=groupOfNames)', attributes=['cn', 'member'])
                    r = ldap.entries
                    if ldap_conf['search'].format(user.name) in r[0]['member']:
                        allowed.append(item)
                    ldap.unbind()
            return allowed
