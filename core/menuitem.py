class MenuItem:
    """Class representing menu items

    These are the items on the side menu of the app
    """

    def __init__(self, text, icon, link, parent=None):
        self.text = text
        self.icon = icon
        self.link = link
        self.parent = parent
