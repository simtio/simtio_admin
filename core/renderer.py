import jinja2
import logging
import markdown as md


class Renderer:
    """ TODO
    """

    def __init__(self, defaults, searchpath=[]):
        self.defaults = defaults
        self.searchpath = searchpath
        return

    def render(self, template, params=None):
        """
        TODO
        :param template: TODO
        :param params: TODO
        """
        logging.debug("Render template " + template)
        if params is None:
            params = {}
        f = open(template, 'r', encoding='utf-8')
        templateLoader = jinja2.FileSystemLoader(searchpath=["./templates/"] + self.searchpath)
        templateEnv = jinja2.Environment(loader=templateLoader, extensions=['jinja2.ext.do'])
        templateEnv.filters['markdown'] = lambda text: jinja2.Markup(md.markdown(text, extensions=['extra']))
        t = templateEnv.from_string(f.read())
        params.update(self.defaults)
        return t.render(params)
