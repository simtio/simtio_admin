import json
from singleton_decorator import singleton
import os.path


@singleton
class Config:
    """
    App configuration management class
    """

    def __init__(self, file=None):
        self.configs = {}
        self.file = file
        self.load(file)

    def get(self, key, default=None):
        """
        Get a configuration value
        :param key: config key
        :param default: default value
        :return: config value
        """
        if key in self.configs:
            return self.configs[key]
        else:
            return default

    def set(self, key, value):
        """
        Set a configuration value
        :param key: config key
        :param value: the config value
        """
        self.configs[key] = value
        self.save(self.file)

    def load(self, file):
        """
        Load a configuration file
        :param file: path to the config file
        """
        if os.path.isfile(file):
            with open(file, 'r') as f:
                self.configs = json.load(f)

    def save(self, file):
        """
        Save a configuration file from actual config
        :param file: path to save the config file
        """
        with open(file, 'w') as f:
            json.dump(self.configs, f)
