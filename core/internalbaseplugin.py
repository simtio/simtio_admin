from core.baseplugin import BasePlugin


class InternalBasePlugin(BasePlugin):
    def __init__(self):
        super(InternalBasePlugin, self).__init__()
        self.globalConfigs = None

    def register_global_config(self, html, config):
        self.globalConfigs = {'html': html, 'config': config}

    def get_global_configs(self):
        return self.globalConfigs
