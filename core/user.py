class User:
    """ Class representing the application user
    """

    def __init__(self, name, first_name=None, last_name=None, display_name=None, email=None, ldap=False):
        self.name = name
        self.first_name = first_name
        self.last_name = last_name
        self.display_name = display_name
        self.email = email
        self.ldap = ldap
        self.groups = []

    def set_groups(self, groups):
        self.groups = groups
