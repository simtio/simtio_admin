import os
import sys
from bottle import static_file, request
from core.page import Page
from core.strings import Strings
from core.menuitemmanager import MenuItemManager
from core.renderer import Renderer
from core.helpers.session import Session
from core.config import Config
from core.widgetmanager import WidgetManager
from core.docker_manager import DockerManager
from jinja2 import Environment
from core.helpers.flash import Flash


class BasePlugin:
    """
    Abstract class for plugins
    """

    def __init__(self):
        self.pages = []
        self.menuItems = []
        self.widgets = []
        self.wizard = []
        self.register_page(
            Page('assets/<filepath:path>',
                 Page.METHOD_GET,
                 self.assets)
        )
        self.strings = Strings(self)
        self.renderer = Renderer({'strings': self.strings, 'menu_item': MenuItemManager().get_menuItem_list()},
                                 [os.path.dirname(sys.modules[self.__class__.__module__].__file__) + '/views/'])

        self.config = self.PluginConfig(Config(), self.get_plugin_type(), self.get_plugin_name())

    def get_plugin_name(self):
        """
        Get the plugin name
        :return: the plugin name
        """
        return os.path.dirname(sys.modules[self.__class__.__module__].__file__).split(os.sep)[-1]

    def get_plugin_type(self):
        """
        Get the plugin type
        :return: the plugin type (internal / external)
        """
        return os.path.dirname(sys.modules[self.__class__.__module__].__file__).split(os.sep)[-2]

    def register_page(self, page):
        """
        Add a new page to the plugin
        :param page: the 'Page' object to add
        """
        if page in self.pages:
            return
        self.pages.append(page)

    def register_menu_item(self, menuitem):
        """
        Add a new item to the plugin
        :param menuitem: the 'MenuItem' object to add
        """
        if menuitem in self.menuItems:
            return
        self.menuItems.append(menuitem)
        MenuItemManager().append_menuItem(menuitem)

    def register_widget(self, widget):
        """
        Add a new widget to the plugin
        :param menuitem: the new 'Widget' object to add
        """
        if widget in self.widgets:
            return
        self.widgets.append(widget)
        WidgetManager().append_widget(widget)

    def register_wizard(self, wizard):
        """
        Add a new wizard question
        :param wizard:  the new Wizard
        """
        self.wizard.append(wizard)

    def has_wizard(self):
        """
        Check if brick has wizard
        :return: boolean
        """
        return self.get_next_wizard_question() is not None

    def get_next_wizard_question(self):
        """
        Get next wizard question
        :return: Wizard object
        """
        for question in self.wizard:
            if self.config.get(question.config_key, None) is None:
                if question.parent is None:
                    return question
                else:
                    if self.config.get(question.parent) == question.parent_response:
                        return question
        return None

    def render(self, view, params=None, current_active_menu_item=None):
        """
        Render a page
        :param view: the page name
        :param params: the page parameters
        :param current_active_menu_item: TODO
        """
        if params is None:
            params = {}
        if current_active_menu_item is None:
            current_active_menu_item = request.path
        params.update({
            'current_user': Session.get("user"),
            'current_lang': Config().get('locale'),
            'current_url': request.path,
            'current_active_menu_item': current_active_menu_item,
            "flash": Flash,
            "simtio_version": Config().get('version')
        })
        return self.renderer.render(os.path.dirname(
                sys.modules[self.__class__.__module__].__file__)
            + '/views/'
            + view
            + '.html', params)

    def assets(self, filepath):
        """
        Serve plugin assets
        :param filepath: assets path
        """
        m, c, C = self.__class__.__module__.split('.')
        return static_file(filepath, root='{0}/{1}/assets'.format(m, c))

    def __del__(self):
        for item in self.menuItems:
            MenuItemManager().remove_menuItem(item)

        for widget in self.widgets:
            WidgetManager().remove_widget(widget)

    def deploy(self):
        """ Deploy the plugin's service """
        with open(os.path.dirname(sys.modules[self.__class__.__module__].__file__) + "/docker-compose.yml") as f:
            with open(Config().get('data_location') +
                      "/" +
                      self.get_plugin_type() +
                      "_" + self.get_plugin_name() +
                      "_compose.yml", "w") as o:
                o.write(Environment().from_string(f.read()).render(version=self.get_version(),
                                                                   config=self.config,
                                                                   global_config=Config()))
                o.close()
            f.close()

            return DockerManager().deploy_stack(Config().get('data_location') +
                                                "/" +
                                                self.get_plugin_type() +
                                                "_" + self.get_plugin_name() +
                                                "_compose.yml", self.get_plugin_type() +
                                                "_" +
                                                self.get_plugin_name()
                                                )

    def install(self):
        """ Check the service deployemnt file before deploying """
        if os.path.isfile(os.path.dirname(sys.modules[self.__class__.__module__].__file__) + "/docker-compose.yml"):
            self.deploy()

    def uninstall(self):
        """ Uninstall the service """
        DockerManager().remove_stack(self.get_plugin_type() + "_" + self.get_plugin_name())

    def get_img(self, name=None):
        if name is None:
            path = os.path.dirname(sys.modules[self.__class__.__module__].__file__) + '/.version'
        else:
            path = 'plugins/{0}/.version'.format(name)
        with open(path, 'r') as vfile:
            lines = vfile.readlines()
            return lines[1].split('=')[1]

    def get_version(self):
        with open(os.path.dirname(sys.modules[self.__class__.__module__].__file__) + '/.version', 'r') as vfile:
            lines = vfile.readlines()
            return lines[0].split('=')[1]

    class PluginConfig:
        """ TODO """

        def __init__(self, config, type, name):
            self.config = config
            self.type = type
            self.name = name

            self.data = self.config.get(self.type, {})
            if self.name not in self.data:
                self.data[self.name] = {}

        def get(self, key=None, default=None):
            """
            TODO
            :param key: plugin name
            :return: the plugin
            """
            if key is None:
                return self.data[self.name]

            if key in self.data[self.name]:
                return self.data[self.name][key]
            else:
                return default

        def set(self, key, value):
            """
            TODO
            :param key: plugin name
            :param value: plugin data
            """
            self.data[self.name][key] = value
            self.commit()

        def update(self, d):
            self.data[self.name].update(d)
            self.commit()

        def commit(self):
            """ TODO """
            d = self.config.get(self.type, {})
            d[self.name] = self.data[self.name]
            self.config.set(self.type, d)
