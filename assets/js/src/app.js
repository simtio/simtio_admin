var $ = require('jquery');

(function() {
    var burger = document.querySelector('.burger');
    if (burger) {
        var menu = document.querySelector('#' + burger.dataset.target);
        burger.addEventListener('click', function() {
        burger.classList.toggle('is-active');
        menu.classList.toggle('is-active');

        init_menu($('.menu-list .is-active'));
    });
    }
})();

$('.drop').on('click', function(event) {
    if ($(this).parent().next('ul:hidden').length === 0) {
        $(this).parent().next('ul').hide();
        $(this).html('<i class="fas fa-angle-down"></i></span>')
    }
    else {
        $(this).parent().next('ul').show();
        $(this).html('<i class="fas fa-angle-up"></i></span>')
    }
    event.preventDefault()
});

function init_menu(item) {
    if (!$(item).hasClass('menu-list')) {
        init_menu($(item).parent())
    }
    if ($(item[0]).is('ul')) {
        $(item[0]).show();
        if (!$($(item[0]).parent()).is('aside'))
            $($(item[0]).parent().find('.drop')[0]).html('<i class="fas fa-angle-up"></i></span>');
    }
}


// Gestion des fenêtres modales

var closeModal = function () {
   var parent = null;
   do {
      if (parent == null) {
         parent = $(this).parent()
      }
      else {
         parent = parent.parent()
      }
   } while(!parent.hasClass("modal"));
   parent.removeClass("is-active");
}

var openModal = function (e) {
    e.preventDefault();
   modal = $(this).attr("data-target");
   $("#"+modal).addClass("is-active");
}

$(document).ready(function() {
   $(".modal-close, .close-modal").click(closeModal);
   $(".modal-button").click(openModal);
});


// Notification
document.addEventListener('DOMContentLoaded', () => {
  (document.querySelectorAll('.notification .delete') || []).forEach(($delete) => {
    $notification = $delete.parentNode;
    $delete.addEventListener('click', () => {
      $notification.parentNode.removeChild($notification);
    });
  });
});