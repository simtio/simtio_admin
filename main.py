from core.app import App
import argparse
import socket

parser = argparse.ArgumentParser()
parser.add_argument("--listen", "-l", help="Simtio address", default="127.0.0.1")
parser.add_argument("--port", "-p", help="Simtio port", default=8080)
parser.add_argument("--worker", "-w", help="Simtio worker ip", default=socket.gethostbyname(socket.gethostname()))
parser.add_argument('--docker', '-d', help="Docker host", default=None)
args = parser.parse_args()

app = App(['internal_plugins', 'plugins'], 'data', args.listen, args.port, args.worker, args.docker)
app.init()
app.run()
