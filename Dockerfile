#############################
## Dockerfile Simtio Admin ##
## SIMTIO                  ##
#############################

FROM simtio/base_image:1.0-buster

ENV PYTHONUNBUFFERED=1

ADD dist/simtio /simtio

WORKDIR /simtio

RUN update-ca-certificates -v -f \
  && curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - \
  && echo "deb https://download.docker.com/linux/debian buster stable" \
    > /etc/apt/sources.list.d/docker.list \
  && apt-get update \
  && apt-get install --no-install-recommends --no-install-suggests -qy python3 python3-pip python3-setuptools python3-wheel \
    gnupg2 docker-ce \
  && pip3 install -r requirements.txt \
  && apt-get clean \
  && rm -rf /tmp/* /var/tmp/* /var/lib/apt/lists/* \
  ;

EXPOSE 8080

CMD ["sh", "-c", "python3 main.py -l 0.0.0.0 -w ${WORKER} -d ${DOCKER}"]
