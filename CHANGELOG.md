# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.5.2] - 2020-05-10
### Added
- articles

## [1.5.1] - 2020-04-16
### Changed
- monitoring labels 
- monitoring colors
- monitoring fix when container crash

## [1.5.0] - 2020-04-15
### Changed
- Add localized brick description
### Removed
- Pack tab in brick view

## [1.4.10] - 2020-04-15
### Fixed
- https://gitlab.com/simtio/simtio_admin/-/issues/9

## [1.4.9] - 2020-03-20
### Changed
- improved monitoring again

## [1.4.8] - 2020-03-19
### Changed
- improved monitoring

### Added
- docker exec from service

## [1.4.7] - 2020-02-24
### Fixed
- Add error log message when brick install failed

## [1.4.6] - 2020-02-22
### Fixed
- Fix Bulma version
- git package-lock.json

## [1.4.5] - 2020-02-21
### Fixed
- Login log french translation

## [1.4.4] - 2020-02-21
### Fixed
- User dashboard search input placeholder text

## [1.4.3] - 2020-02-21
### Added
- Licence information

## [1.4.2] - 2020-02-18
### Fixed
- Plugins names without "_" in brick view

## [1.4.1] - 2020-02-18
### Fixed
- Input background color in modal
- Label color
- Config page titles
- input text color
### Added
- Titles and dividers in config page
### Removed
- input placeholders

## [1.4.0] - 2020-02-18
### Added
- brick install visualisation

## [1.3.1] - 2020-02-17
### Fixed
- History page translation

## [1.3.0] - 2020-02-17
### Added
- Version number in footer
- Localized footer

## [1.2.0] - 2020-02-15
### Added
- ActionLogger class and history plugin

## [1.1.2] - 2019-12-20
### Fixed
- Plugin update (old menu items and widget not deleted, module not reloaded)

## [1.1.1] - 2019-12-19
### Added
- PluginConfig update function

### Changed
- PluginConfig get all
- Base image version

## [1.1.0] - 2019-12-18
### Added
- CI
