import unittest
from core.config import Config
import os


class TextConfig(unittest.TestCase):
    def setUp(self):
        self.key = "foo"
        self.val = "bar"
        Config("data/config_test.json")

    def tearDown(self):
        if os.path.isfile("data/config_test.json"):
            os.remove("data/config_test.json")

    def test_get_none(self):
        self.assertIsNone(Config().get("test"))

    def test_get(self):
        Config().set(self.key, self.val)
        self.assertEqual(Config().get(self.key), self.val)
