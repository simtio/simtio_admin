import unittest
from core.widget import Widget


class TestWidget(unittest.TestCase):
    def setUp(self):
        self.name = "test"
        self.html = "<span>test</span>"
        self.widget = Widget(self.name, self.html)

    def test_name(self):
        self.assertEqual(self.widget.name, self.name)

    def test_html(self):
        self.assertEqual(self.widget.html, self.html)
