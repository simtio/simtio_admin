import unittest
from core.pluginmanager import PluginManager
from core.baseplugin import BasePlugin


class TestPluginManager(unittest.TestCase):

    def test_load_plugins(self):
        pm = PluginManager(['internal_plugins', 'plugins'])
        pm.load_plugins()
        for name, plugin in pm.plugins.items():
            self.assertIsInstance(plugin, BasePlugin)

    def test_get_plugin(self):
        pm = PluginManager(['internal_plugins', 'plugins'])
        pm.load_plugins()
        for name, plugin in pm.plugins.items():
            pl = pm.get_plugin(name)
            self.assertIsInstance(pl, BasePlugin)
