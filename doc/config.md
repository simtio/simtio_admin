# Config
Singleton __Config__

```Config().get(key, default=None)``` : Récupération d'une clé

```Config().set(key, value)``` : Définit une clé