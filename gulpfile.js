var gulp = require('gulp');
var browserify = require('gulp-browserify');
var sass = require('gulp-sass');
var fs = require('fs');
const tar = require('gulp-tar-path');
const gzip = require('gulp-gzip');
const package = require('./package.json');
const gunzip = require('gulp-gunzip');
const untar = require('gulp-untar');
const clean = require('gulp-clean');
const exec = require('child_process').exec;

gulp.task('js', function() {
    // Single entry point to browserify
    return gulp.src('./assets/js/src/app.js')
        .pipe(browserify({
          insertGlobals : true,
          debug : true
        }))
        .pipe(gulp.dest('./assets/js/'))
});

gulp.task('sass', function () {
  return gulp.src('./assets/css/scss/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./assets/css'));
});


fs.readdirSync('./internal_plugins').forEach(file => {
  gulp.task('sass_' + file, function() {
      return gulp.src('./internal_plugins/' + file + '/assets/css/scss/**/*.scss', { allowEmpty: true })
    .pipe(sass({includePaths: ['./assets/css/scss/*']}).on('error', sass.logError))
    .pipe(gulp.dest('./internal_plugins/' + file + '/assets/css'));
  })
    gulp.task('js_' + file, function() {
        return gulp.src('./internal_plugins/' + file + '/assets/js/src/app.js', { allowEmpty: true })
        .pipe(browserify({
          insertGlobals : true,
          debug : true
        }))
        .pipe(gulp.dest('./internal_plugins/' + file + '/assets/js/'))
    })
})


var plugins_js = [];
var plugins_sass = [];
    fs.readdirSync('./internal_plugins').forEach(file => {
  plugins_js.push("js_"+file)
  plugins_sass.push("sass_"+file)
})
gulp.task('plugins_js', gulp.series(...plugins_js))
gulp.task('plugins_sass', gulp.series(...plugins_sass))


gulp.task('watch', function(){
    gulp.watch('./assets/css/scss/**/*.scss', gulp.series('sass'));
    gulp.watch('./assets/js/src/app.js', gulp.series('js'));
    fs.readdirSync('./internal_plugins').forEach(file => {
        gulp.watch('./internal_plugins/' + file + '/assets/css/scss/**/*.scss', gulp.series('sass_' + file));
        gulp.watch('./internal_plugins/' + file + '/assets/js/src/app.js', gulp.series('js_' + file));
    });
})

gulp.task('tar', function() {
    return gulp.src(['package.json', 'templates', 'locales', 'plugins/.gitkeep', 'locales', 'internal_plugins/**/assets/css/*.css', 'internal_plugins/**/assets/js/*.js', 'internal_plugins/**/assets/images', 'internal_plugins/**/locales', 'internal_plugins/**/views', 'internal_plugins/**/*.py', 'data/.gitkeep', 'core', 'assets/css/*.css', 'assets/js/*.js', 'assets/images', 'requirements.txt', "main.py",])
    .pipe(tar('simtio_' + package.version + '.tar'))
    .pipe(gzip())
    .pipe(gulp.dest('dist'))
});

gulp.task('untar', function() {
   return gulp.src('dist/simtio_' + package.version + '.tar.gz')
    .pipe(gunzip())
    .pipe(untar())
    .pipe(gulp.dest('dist/simtio'))
});

gulp.task('clean', function () {
  return gulp.src(['dist', 'assets/css/*.css', 'assets/js/*.js', 'internal_plugins/**/assets/css/*.css', 'internal_plugins/**/assets/js/*.js'], {read: false, allowEmpty: true})
    .pipe(clean());
});

gulp.task('docker_image', function() {
  return exec('docker build -t registry.gitlab.com/simtio/simtio_admin/simtio:' + package.version + " .", function(err) {
    if (err) {
        console.log(err);
    }
  });
});

gulp.task('build', gulp.series('sass', 'js', 'plugins_sass', 'plugins_js'));

gulp.task('dist', gulp.series('clean', 'build', 'tar'));

gulp.task('docker', gulp.series('dist', 'untar', 'docker_image'));