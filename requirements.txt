bottle == 0.12.13
jinja2 == 2.10
singleton-decorator == 1.0.0
beaker == 1.10.0
minio == 4.0.10
password-strength == 0.0.3.post2
docker == 4.0.2
ldap3==2.6.1
Markdown == 3.1.1
tinydb == 3.15.2