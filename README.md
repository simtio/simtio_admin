# Installation

*  Get Simtio-Admin :
```
git clone https://gitlab.com/simtio/simtio_admin.git
```

*  Get Python3 and pip3 :
```
# Debian-based systems :
apt-get install python3 python3-pip

# Redhat-based systems :
yum install python3 python3-pip
```

*  Install python requirements :
```
pip3 install -r requirements.txt
```

*  Install and build nodejs requirements :
```
npm install && npm audit fix && npm run build
```

*  Run the main binary :
```
python3 main.py
```

# Development

## internal bricks

### scss dependencies
```sass
@import '../../../../assets/css/scss/_colors';
```
### Docker build
Multiarch :
```bash
docker buildx build -t registry.gitlab.com/simtio/simtio_admin/simtio:latest -t registry.gitlab.com/simtio/simtio_admin/simtio:0.x --platform=linux/aarch64,linux/amd64,linux/arm . --push
```

## Debug log
To show debug log add `"log_level": "DEBUG"` in your config file.

# Docker Run
```bash
 docker run -v simtio_data:/simtio/data -v /var/run/docker.sock:/var/run/docker.sock -p8080:8080 -e WORKER=<host ip> -e DOCKER=unix:///var/run/docker.sock registry.gitlab.com/simtio/simtio_admin/simtio:1.0.0
```
