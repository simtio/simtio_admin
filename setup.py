import setuptools

setuptools.setup(
    name="simtio_admin",
    version="0.0.2",
    author="Simtio",
    author_email="conctact@simtio.charly-hue.fr",
    description="Simtio admin",
    long_description="Simtio admin",
    long_description_content_type="text/markdown",
    url="https://gitlab.com/simtio/simtio_admin",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
