var Masonry = require('masonry-layout');

var msnry = new Masonry( '.grid', {
  itemSelector: '.grid-item',
  // use element for option
  columnWidth: 180,
//  percentPosition: true,
  fitWidth: true,
  gutter: 10
});