from internal_plugins.user_dashboard.search_motors.search_motor import SearchMotor


class Youcare(SearchMotor):
    name = "YouCare"
    url = "https://youcare.world/all"
    param = "q"
