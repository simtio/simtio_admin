from internal_plugins.user_dashboard.search_motors.search_motor import SearchMotor


class Lilo(SearchMotor):
    name = "Lilo"
    url = "https://search.lilo.org/results.php"
    param = "q"
