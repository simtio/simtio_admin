from internal_plugins.user_dashboard.search_motors.search_motor import SearchMotor


class Duckduckgo(SearchMotor):
    name = "Duckduckgo"
    url = "https://duckduckgo.com/"
    param = "q"
