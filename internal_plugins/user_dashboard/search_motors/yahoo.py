from internal_plugins.user_dashboard.search_motors.search_motor import SearchMotor
from core.config import Config


class Yahoo(SearchMotor):
    name = "Yahoo"
    if Config().get("locale") != "en":
        url = "https://{0}.search.yahoo.com/search".format(Config().get("locale"))
    else:
        url = "https://search.yahoo.com/search"
    param = "q"
