from internal_plugins.user_dashboard.search_motors.search_motor import SearchMotor


class Ecogine(SearchMotor):
    name = "Ecogine"
    url = "https://ecogine.org/"
    param = "q"
