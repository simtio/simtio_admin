from internal_plugins.user_dashboard.search_motors.search_motor import SearchMotor


class Qwant(SearchMotor):
    name = "Qwant"
    url = "https://qwant.com/"
    param = "q"
