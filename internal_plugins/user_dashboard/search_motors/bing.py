from internal_plugins.user_dashboard.search_motors.search_motor import SearchMotor


class Bing(SearchMotor):
    name = "Bing"
    url = "https://bing.com/search"
    param = "q"
