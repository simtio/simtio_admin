from internal_plugins.user_dashboard.search_motors.search_motor import SearchMotor


class Google(SearchMotor):
    name = "Google"
    url = "https://google.fr/search"
    param = "q"
