from internal_plugins.user_dashboard.search_motors.search_motor import SearchMotor


class Ecosia(SearchMotor):
    name = "Ecosia"
    url = "https://ecosia.org/search"
    param = "q"
