import inspect
import sys

from bottle import request

from core import InternalBasePlugin, Page, UserDashboardManager, UserDashboardItem
from internal_plugins.user_dashboard.search_motors import * # NOQA
from core.helpers import Session


class Plugin(InternalBasePlugin):
    def __init__(self):
        super(Plugin, self).__init__()
        self.register_page(Page('', Page.METHOD_GET, self.action_index))

        UserDashboardManager().append_item(UserDashboardItem("Simtio", "/assets/images/logo_no_name.png", "/internal_plugins/dashboard/"))

        self.register_global_config(self.action_config, self.action_config_process)

    def action_index(self):
        user = Session.get("user")

        return self.render('index', {
            "search_motor": eval(self.config.get("search_motor", "Google")),
            "items": UserDashboardManager().get_allowed(user)
        })

    def action_config(self):
        return self.render('config', {
            "search_motor": self.config.get("search_motor", "Google"),
            "search_motors": self.getSearchMotorsList()
        })

    def action_config_process(self):
        request.body.read()
        search_motor = request.forms.get('search_motor')
        if self.config.get("search_motor", None) != search_motor:
            self.config.set("search_motor", search_motor)
            return self.strings.success
        else:
            return ""

    def getSearchMotorsList(self):
        clsmembers = inspect.getmembers(sys.modules["internal_plugins.user_dashboard.search_motors"], inspect.isclass)
        slist = []
        for item in clsmembers:
            n, obj = item
            slist.append({"value": n, "text": eval(n).name})

        return slist
