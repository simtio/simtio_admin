var Chart = require('chart.js');
var Masonry = require('masonry-layout');


function createChart(gdata){
    var opts =
    {
        rotation: 1 * Math.PI,
        circumference: 1 * Math.PI,
        animation: false,
        tooltips: {
            enabled: false
        }
    }


    new Chart($('#'+gdata.id+'-cpu'), {
        type: 'doughnut',
        data: {
          datasets: [{
            label: "Utilisation CPU",
            backgroundColor: ["#3e95cd", "#385169"],
            data: [gdata.cpu, 100 - gdata.cpu]
          }],
          labels: [ ]
        },
        options: opts
    });

    new Chart($('#'+gdata.id+'-ram'), {
        type: 'doughnut',
        data: {
          datasets: [{
            label: "Utilisation RAM",
            backgroundColor: ["#3e95cd", "#385169"],
            data: [gdata.ram / gdata.total_mem * 100 , (gdata.total_mem - gdata.ram) / gdata.total_mem *100 ]
          }],
          labels: [ ]
        },
        options: opts
    });
    }

function createBarChart(gdata)
{
    var opts =
    {
        animation: false,
        tooltips: {
            display: false
        },
        legend: { display: false },
        responsive: false,
        maintainAspectRatio: false,
        scales: {
          xAxes: [{ 
              gridLines: { display: false },
              stacked: true,
              display: false
            }],
          yAxes: [{ 
            gridLines: { display: false },
            stacked: true,
            display: false 
            }]
        }
     }
     
    var chart = new Chart($('#'+gdata.id+'-cpu'), 
    {
        type: 'horizontalBar',
        data: {
        datasets: [
        {
             label: "",
             backgroundColor: ["#3e95cd", "#3e95cd"],
             data: [gdata.cpu, gdata.ram / gdata.total_mem * 100],
             barThickness: 30,
             borderColor: 'rgba(255, 255, 255, 1)',
             borderWidth: 1
        },
        {
            label: "",
            backgroundColor: ["#385169", "#385169"],
            data: [ 100 - gdata.cpu , (gdata.total_mem - gdata.ram) / gdata.total_mem *100 ],
            barThickness: 30,
            borderColor: 'rgba(255, 255, 255, 1)',
            borderWidth: 1
        }],
        labels: [ 'CPU', 'RAM']
        },
        options: opts
     });
}

//////////////////
// Monitor stack
//////////////////
var Mustache = require('mustache');
var $ = require('jquery');

var template = ""

function getData(){
    $.getJSON(
        url = "data"
    ).done( (result) => {
        Mustache.parse(template);
        var text = Mustache.render(template, result);
        $("#monitoring").html(text);
        
        $("button#modal-button").on('click', openModal);
        $("#close-modal-button").on('click', closeModal);
        $('#filter-field').on('input', filter);
        $("button#start-button").on('click', start);
        $("button#stop-button").on('click', stop);

        result.nodes.forEach( (item, index) => {
            createChart(item);
          });

        result.services.forEach( (item, index) => {
            createBarChart(item);
          });

    }).fail( () => {
        console.log("failed to get json");
    });
}


$(document).ready(() => {
    $.get(
        url="template"
    ).done( (result) => {
     template = result;
     //getData();
     setInterval(getData,250);

    }).fail( (obj, status, err) =>{
        console.log(err)
    });
});


/////////
//GRID
/////////
var msnry = new Masonry( '.grid', {
    itemSelector: '.grid-item',
    columnWidth: '.grid-sizer',
    percentPosition: true,
    gutter: 10
  });

/////////
// Modal
/////////



var closeModal = function () {
    $(this.parentNode.parentNode.parentNode).removeClass("is-active");
 }
 
 var openModal = function () {
    modal = $(this).attr("data-target");
    c = $(this).attr("service");
    nom = $(this).attr("name");

    $("#"+modal).addClass("is-active");

    $.getJSON(
        url = "logs/" +  c
    ).done( (result) => {
        $('#'+modal).find('p').first().html('Logs: '+nom);
        $('#'+modal).find('section').html(result);
    })
    .fail( (err) => {
        $('#'+modal).find('p').first().html('Logs: '+nom);
        $('#'+modal).find('section').html(err.responseText);
    });

    
 }

 //////////
 //Filter
 //////////

 var filter = function(){
     var f = $(this).val();
    var list = $('#'+modal).find('section').find('p');
    list.each( (idx, el) => {
        var n = $(el).html();
        if (n.includes(f)){
            el.setAttribute("style", "display:block");
         }
         else{
            el.setAttribute("style", "display:none");
         }
    });
 }

 //////////
 //Service
 //////////

 var start = function(){
    c = $(this).attr("service");
    $.getJSON(
        url = "start/" +  c
    ).done( (result) => {
        console.log("service started");
    })
    .fail( (err) => {
        console.log("failed to start service");
    });
 }

 var stop = function() {
    c = $(this).attr("service");
    $.getJSON(
        url = "stop/" +  c
    ).done( (result) => {
        console.log("service started");
    })
    .fail( (err) => {
        console.log("failed to start service");
    });
 }