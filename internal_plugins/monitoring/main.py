from core.baseplugin import BasePlugin
from core.menuitem import MenuItem
from core.page import Page
from core.docker_manager import DockerManager
import json


class Plugin(BasePlugin):

    data = {}
    log = ""
    client = None

    def __init__(self):
        super(Plugin, self).__init__()

        self.register_menu_item(MenuItem(self.strings.health, 'fas fa-heartbeat',   "/internal_plugins/monitoring/"))

        self.register_page(Page('', Page.METHOD_GET, self.action_monitoring))
        self.register_page(Page('data', Page.METHOD_GET, self.action_data))
        self.register_page(Page('template', Page.METHOD_GET, self.action_template))
        self.register_page(Page('logs/<container>', Page.METHOD_GET, self.action_logs))
        self.register_page(Page('start/<service>', Page.METHOD_GET, self.action_start))
        self.register_page(Page('stop/<service>', Page.METHOD_GET, self.action_stop))

    def action_start(self, service):
        DockerManager().start_service(service)

    def action_stop(self, service):
        DockerManager().stop_service(service)

    def getData(self):
        DockerManager().thread_service_data()
        nodes = DockerManager().nodes
        services = DockerManager().services

        mst_service = []
        mst_nodes = []

        for k, v in nodes.items():
            cpu = float("%.2f" % v['cpu'])
            mem = float("%.2f" % v['mem'])
            tot = float("%.2f" % v['total_mem'])
            mem_ratio = mem / tot * 100

            has_alarm = False
            has_warning = False
            if(mem_ratio > 75 or cpu > 75):
                has_alarm = True
            elif(mem_ratio > 75 or cpu > 50):
                has_warning = True

            mst_nodes.append({'nom': k, 'cpu': cpu, 'ram': mem, 'total_mem': tot, 'has_warning': has_warning, 'has_alarm': has_alarm, 'id': v['id']})

        for k, v in services.items():
            noms = k.replace('plugins_', '').split('_')
            nom = '_'.join(noms[:len(noms)//2])
            status = v["status"]

            cpu = float("%.2f" % v['cpu'])
            mem = float("%.2f" % v['mem'])
            tot = float("%.2f" % v['total_mem'])
            mem_ratio = mem / tot * 100
            if(mem_ratio > 75 or cpu > 75):
                has_alarm = True
            elif(mem_ratio > 50 or cpu > 50):
                has_warning = True

            mst_service.append(
                {'nom': nom, 'cpu': cpu, 'ram': mem, 'url': self.get_img(nom),
                    'total_mem': tot, 'has_warning': has_warning, 'has_alarm': has_alarm,
                    'id': nom, 'service': k, 'is-started': status == 'started',
                    'is-stopped': status == 'stopped', 'is-working': status == 'working'})

        return {
            "nodes": mst_nodes,
            "services": mst_service,
            "node_lbl": self.strings.services,
            "service_lbl": self.strings.nodes
        }

    def action_logs(self, container):
        logs = DockerManager().get_logs(container).decode('utf-8').split('\n')
        return "".join(['<p>'+log+'</p>' for log in logs])

    def action_monitoring(self):
        return self.render("monitoring", {})

    def action_template(self):
        with open('internal_plugins/'+self.get_plugin_name()+'/views/service_template.mst') as tfile:
            return tfile.read()

    def action_data(self):
        return json.dumps(self.getData())
