from core.baseplugin import BasePlugin
from core.widgetmanager import WidgetManager
from core.menuitem import MenuItem
from core.page import Page
import os


class Plugin(BasePlugin):
    """ Plugin dashboard class

    Display all widgets and article
    """

    articles = []

    def __init__(self):
        super(Plugin, self).__init__()

        self.register_menu_item(MenuItem(self.strings.title, 'fas fa-star',   "/internal_plugins/dashboard/"))

        self.register_page(Page('', Page.METHOD_GET, self.action_dashboard))
        self.register_page(Page('<numero>', Page.METHOD_GET, self.action_article))

        self.articles = self.get_article_list()

    def get_article_list(self):
        """ Get the list of all articles
        Parse the dashboard plugin folder to list article HTML files
        :return: article list
        """
        al = []
        path = 'internal_plugins/dashboard/views/help'
        for r, d, file in os.walk(path):
            if r != path:
                continue
            for f in file:
                name = f.split('.')[0]
                al.append(name)
        return al

    def action_article(self, numero):
        """
        Render the full article
        :param numero: article number (from html file name)
        """
        return self.render('help/'+numero, {})

    def action_dashboard(self):
        """
        Render widgets and article previews into the dashboard
        """
        self.articles = self.get_article_list()

        return self.render("dashboard", {
            "widgets": WidgetManager().get_widget_list(),
            "articles": ['help/'+a+'.html' for a in self.articles]
            })
