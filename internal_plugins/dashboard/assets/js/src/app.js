var Masonry = require('masonry-layout');

var msnry = new Masonry( '.widget-grid', {
  columnWidth: 300,
  gutter: 7
});