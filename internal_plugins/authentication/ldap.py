from ldap3 import Server, Connection, ALL


class Ldap:
    def __init__(self, server, bind_user, bind_password, search, group):
        self.server = server
        self.bind_user = bind_user
        self.bind_password = bind_password
        self.search = search
        self.group = group

    def check_auth(self, user, password):
        s = Server(self.server, get_info=ALL)
        c = Connection(s, user=self.search.format(user),
                       password=password, check_names=True, lazy=False,
                       raise_exceptions=False)
        c.open()
        c.bind()
        if c.result['result'] == 0:
            success = True
        else:
            success = False
        c.unbind()
        return success

    def check_group(self, user):
        server = Server(self.server)
        conn = Connection(server, user=self.bind_user, password=self.bind_password)
        conn.bind()
        conn.search(self.group, '(objectclass=groupOfNames)', attributes=['cn', 'member'])
        out = False
        if len(conn.entries) > 0:
            for group in conn.entries:
                for guser in group.member:
                    if guser == self.search.format(user):
                        out = True
        conn.unbind()
        return out

    def get_user(self, user):
        server = Server(self.server)
        conn = Connection(server, user=self.bind_user, password=self.bind_password)
        conn.bind()
        conn.search(self.search.format(user), '(objectclass=inetOrgPerson)',
                    attributes=['cn', 'displayName', 'givenName', 'mail', 'sn', 'uid'])
        out = {
            "uid": str(conn.entries[0]['uid']),
            "givenName": str(conn.entries[0]['givenName']),
            "cn": str(conn.entries[0]['cn']),
            "displayName": str(conn.entries[0]['displayName']),
            "mail": str(conn.entries[0]['mail']),
        }
        print(out)

        conn.unbind()
        return out
