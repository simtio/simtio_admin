from ldap3.core.exceptions import LDAPSocketOpenError

from core.baseplugin import BasePlugin
from core.page import Page
from bottle import request, redirect
from core.helpers.session import Session
from core.user import User
import hashlib
from core.config import Config
import logging
from internal_plugins.authentication.ldap import Ldap
from core import ActionLogger


class Plugin(BasePlugin):
    """ Authentication plugin class

    """

    def __init__(self):
        super(Plugin, self).__init__()
        self.register_page(Page('login', Page.METHOD_GET, self.action_login))
        self.register_page(Page('login', Page.METHOD_POST, self.action_login_process))
        self.register_page(Page('logout', Page.METHOD_GET, self.action_logout))
        self.register_page(Page('info', Page.METHOD_GET, self.action_info))
        self.admin_user = "admin"
        self.password = "simtio"

    def action_login(self, error=None):
        """
        Redirect to the user login page
        """
        if Session.get("user") is not None:
            redirect('info')

        return self.render("login", {"error": error})

    def action_login_process(self):
        """
        Login the user
        """
        request.body.read()
        user = request.forms.get("user")
        password = request.forms.get("password")
        if user == self.admin_user \
                and hashlib.sha512(str.encode(password)).hexdigest() == Config().get('admin_password'):
            Session.set('user', User(user, ldap=False))
            ActionLogger().info(self.strings.Connected)
            redirect('/internal_plugins/dashboard/')

        if Config().get('ldap', None) is not None:
            try:
                ldap_conf = Config().get('ldap')
                ldap = Ldap(ldap_conf['server'], ldap_conf['user'], ldap_conf['password'],
                            ldap_conf['search'], ldap_conf['admin_group'])
                if ldap.check_auth(user, password):
                    ldap_user = ldap.get_user(user)
                    Session.set('user', User(ldap_user['uid'], ldap_user['givenName'],
                                             ldap_user['cn'], ldap_user['displayName'], ldap_user['mail'], True))
                    ActionLogger().info(self.strings.Connected)
                    redirect('/internal_plugins/dashboard/')
            except LDAPSocketOpenError as err:
                logging.error("{0} ({1})".format(self.strings.ldap_error, err))
                return self.action_login(self.strings.ldap_error)

        return self.action_login(self.strings.login_error)

    def action_logout(self):
        """
        Logout the user then redirect to the login page
        """
        Session.remove("user")
        redirect("login")

    def action_info(self):
        """
        Redirect to the user info page
        """
        return self.render("info")
