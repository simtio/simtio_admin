from core import BasePlugin, Page, Config, PluginManager, ActionLogger
from bottle import request
import hashlib
from password_strength import PasswordPolicy


class Plugin(BasePlugin):
    """ Plugin config class

    This is the config management plugin
    """

    def __init__(self):
        super(Plugin, self).__init__()
        self.register_page(Page('', Page.METHOD_GET, self.action_view))
        self.register_page(Page('', Page.METHOD_POST, self.action_save))

    def action_view(self, msg=""):
        """
        Render the profile configuration page
        """

        ldap_config = Config().get("ldap", {})
        if 'user' in ldap_config:
            ldap_user = ldap_config['user']
        else:
            ldap_user = ""
        if 'password' in ldap_config:
            ldap_password = "****"
        else:
            ldap_password = ""
        if 'search' in ldap_config:
            ldap_search = ldap_config['search']
        else:
            ldap_search = ""
        if 'admin_group' in ldap_config:
            ldap_admin_group = ldap_config['admin_group']
        else:
            ldap_admin_group = ""
        if 'server' in ldap_config:
            ldap_server = ldap_config['server']
        else:
            ldap_server = ""

        return self.render('index', {
            "ldap_server": ldap_server,
            "ldap_user": ldap_user,
            "ldap_search": ldap_search,
            "ldap_admin_group": ldap_admin_group,
            "ldap_password": ldap_password,
            "msg": msg,
            "plugins_configs": self.get_plugins_configs()
        })

    def action_save(self):
        """
        Saves the configuration
        """
        policy = PasswordPolicy.from_names(
            length=8,  # min length: 8
            uppercase=2,  # need min. 2 uppercase letters
            numbers=2,  # need min. 2 digits
            special=2,  # need min. 2 special characters
            nonletters=2,  # need min. 2 non-letter characters (digits, specials, anything)
        )

        request.body.read()
        lang = request.forms.get('locale')
        current_password = request.forms.get('current_password')
        new_password = request.forms.get('new_password')
        repeat_password = request.forms.get('repeat_password')
        msg = ""
        if current_password and new_password and repeat_password:
            if new_password == repeat_password:
                if hashlib.sha512(str.encode(current_password)).hexdigest() == Config().get('admin_password'):
                    result = policy.test(new_password)
                    if len(result) > 0:
                        for r in result:
                            msg += self.strings.__getattr__("password_error_" + r.name()).format(r.args.__getitem__(0))
                            if r != result[-1]:
                                msg += '<br />'
                    else:
                        Config().set('admin_password', hashlib.sha512(str.encode(new_password)).hexdigest())
                        msg += self.strings.password_changed + '.'
                else:
                    msg += self.strings.bad_password + '.'
            else:
                msg += self.strings.password_dont_match + '.'

        if lang != Config().get('locale'):
            Config().set('locale', lang)
            msg += self.strings.need_restart + '.'

        ldap_config = {
            'server': request.forms.get('ldap_server'),
            'user': request.forms.get('ldap_user'),
            'search': request.forms.get('ldap_search'),
            'admin_group': request.forms.get('ldap_admin_group')
        }
        if request.forms.get('ldap_admin_group') != "" and request.forms.get('ldap_admin_group') != "****":
            ldap_config['password'] = request.forms.get('ldap_admin_group')

        Config().set('ldap', ldap_config)
        msg += self.strings.ldap_saved + '.'

        for plugin in self.get_plugins_configs():
            msg += " " + plugin['config']()

        ActionLogger().info(self.strings.log_changed)

        return self.action_view(msg)

    def get_plugins_configs(self):
        plugins_configs = []
        for plugin in PluginManager().get_internal_plugins():
            plugins_configs.append(plugin.get_global_configs())
        return plugins_configs
