from core.baseplugin import BasePlugin
from core.page import Page
from core.docker_manager import DockerManager
from bottle import request, redirect


class Plugin(BasePlugin):
    """ Plugin debug class

    Provide usefull URLs for docker debuging purpose
    """

    def __init__(self):
        super(Plugin, self).__init__()
        self.docker = DockerManager()
        self.register_page(Page('docker_list', Page.METHOD_GET, self.action_docker_list))
        self.register_page(Page('docker_deploy', Page.METHOD_GET, self.action_docker_deploy))
        self.register_page(Page('docker_deploy', Page.METHOD_POST, self.action_docker_deploy_process))
        self.register_page(Page('docker_delete/<stack>', Page.METHOD_GET, self.action_docker_delete))

    def action_docker_list(self):
        """
        List all docker container
        """
        return self.render('docker_list', {"stacks": self.docker.list_stacks()})

    def action_docker_deploy(self, status=None):
        """
        Monitor docker deployement
        :param status: stack default status
        """
        return self.render('docker_deploy', {'status': status})

    def action_docker_deploy_process(self):
        """
        Deploy a container then monitor
        """
        request.body.read()
        name = request.forms.get("name")
        file = request.forms.get("file")
        d = self.docker.deploy_stack(file, name)

        return self.action_docker_deploy(status=d)

    def action_docker_delete(self, stack):
        """
        Remove a deployed stack
        :param stack: name of the stack
        """
        self.docker.remove_stack(stack)
        redirect('../docker_list')
