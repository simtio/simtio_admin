from core.baseplugin import BasePlugin
from core.page import Page
import json
from core.config import Config
from bottle import request, redirect
import hashlib
from password_strength import PasswordPolicy


class Plugin(BasePlugin):
    """ Plugin setup class

    Set SIMTIO up on the first launch
    """

    def __init__(self):
        super(Plugin, self).__init__()
        self.register_page(Page('lang', Page.METHOD_GET, self.action_lang))
        self.register_page(Page('lang', Page.METHOD_POST, self.action_lang_process))
        self.register_page(Page('password', Page.METHOD_GET, self.action_password))
        self.register_page(Page('password', Page.METHOD_POST, self.action_password_process))
        self.register_page(Page('end', Page.METHOD_GET, self.action_end))

    def check_setup(self, step):
        """
        Check the installation process
        :param step: process step
        """
        if not Config().get('locale'):
            if step != 'lang':
                redirect('lang')
        elif not Config().get('admin_password'):
            if step != 'password':
                redirect('password')
        else:
            if step != 'end':
                redirect('end')

    def action_lang(self):
        """
        App language selection step
        """
        self.check_setup('lang')
        return self.render(
            "lang", {
                'continue_strings': json.dumps(self.strings.get_in_all_lang("continue")),
                'step': 'lang'
            })

    def action_lang_process(self):
        """
        Set the choosen language then redirect to the password step
        """
        self.check_setup('lang')
        request.body.read()
        locale = request.forms.get("locale")
        Config().set('locale', locale)
        redirect('password')

    def action_password(self, error=None):
        """
        Admin password definition step
        """
        self.check_setup('password')
        return self.render('password', {'step': 'password', 'error': error})

    def action_password_process(self):
        """
        Set the choosen password then redirect to final step
        """
        policy = PasswordPolicy.from_names(
            length=8,  # min length: 8
            uppercase=2,  # need min. 2 uppercase letters
            numbers=2,  # need min. 2 digits
            special=2,  # need min. 2 special characters
            nonletters=2,  # need min. 2 non-letter characters (digits, specials, anything)
        )

        self.check_setup('password')
        request.body.read()
        password = request.forms.get("password")
        confirm = request.forms.get("confirm")
        if password != confirm:
            return self.action_password(self.strings.password_error)

        result = policy.test(password)
        if len(result) > 0:
            msg = ""
            for r in result:
                msg += self.strings.__getattr__("password_error_" + r.name()).format(r.args.__getitem__(0))
                if r != result[-1]:
                    msg += '<br />'
            return self.action_password(msg)

        Config().set('admin_password', hashlib.sha512(str.encode(password)).hexdigest())
        redirect('end')

    def action_end(self):
        """
        App installation confirmation
        """
        self.check_setup('end')
        Config().set('installed', True)
        return self.render('end', {'step': 'end'})
