var $ = require('jquery');

$(document).ready(function() {
   $(".modal-close, .close-modal").click(closeModal);
   $(".modal-button").click(openModal);
   $("#filter-field").keyup(filter);
   $('.download_brick_link').click(install_handler)
});


// Gestion des fenêtres modales

var closeModal = function () {
   var parent = null;
   do {
      if (parent == null) {
         parent = $(this).parent()
      }
      else {
         parent = parent.parent()
      }
   } while(!parent.hasClass("modal"))
   parent.removeClass("is-active");
}

var openModal = function () {
   modal = $(this).attr("data-target")
   $("#"+modal).addClass("is-active");
}


// Gestion du filtre

var filter = function () {
   var filter = $(this).val().toLocaleLowerCase();

    $('*[id=filter-criteria]').each(function(i, el) {
      var tile_name = el.textContent.toLowerCase();
      if (tile_name.includes(filter)){
         el.closest('#filter-element').setAttribute("style", "display:flex");
      }
      else{
         el.closest('#filter-element').setAttribute("style", "display:none");
      }
  });

  
}

var install_handler = function(e) {
    e.preventDefault()
    var name = $(this).attr('href')
    $(".brick-modal").removeClass('is-active')
    $("#brick_install_modal .modal-card-title").append(name.split('/')[1])
    $('#brick_install_modal').addClass('is-active')
    setTimeout(function(){
        window.location.replace(name);
    },500);
}

