from core import BasePlugin, Widget, MenuItem, Page, App, Wizard, PluginManager, ActionLogger, Config
from minio import Minio
from bottle import redirect, request
import ast
import tarfile
import os
# import threading
from internal_plugins.briques.utils import bformat, parse_local_bricks
import shutil
import logging


class Plugin(BasePlugin):
    """ Plugin library class

    This is the plugin for SIMTIO library interface.
    It allow to install, remove or update plugins.
    """

    bricks = []
    ROWS = 9
    LINES = 5

    def __init__(self):
        super(Plugin, self).__init__()

        self.register_widget(Widget(self.strings.bricks, self.widget_nb_briques))
        self.register_widget(Widget(self.strings.updates, self.widget_nb_updates))

        self.register_menu_item(MenuItem(self.strings.Bricks, 'fas fa-cubes', "/internal_plugins/briques/"))

        self.register_page(Page('', Page.METHOD_GET, self.action_briques))
        self.register_page(Page('download/<name>', Page.METHOD_GET, self.action_download))
        self.register_page(Page('remove/<name>', Page.METHOD_GET, self.action_remove))
        self.register_page(Page('', Page.METHOD_POST, self.action_briques_process))

        # TODO: voir le slug

    def widget_nb_briques(self):
        """
        Build the widget counting the number of installed bricks
        """
        return self.render("widgets/nb_bricks", {"nb_briques": len(parse_local_bricks("plugins"))})

    def widget_nb_updates(self):
        """
        Build the widget counting the number of updated bricks
        """
        self._update_briques()
        nb_updates = 0
        for brick in self.bricks:
            if 'current_version' in brick:
                if brick['current_version'] < brick['latest_version']:
                    nb_updates += 1

        return self.render("widgets/nb_updates", {"nb_updates": nb_updates})

    def _update_briques(self):
        """
        Update the local list of bricks
        """
        client = Minio('minio.simtio.charly-hue.fr', access_key='', secret_key='', secure=True)
        _bricks = client.get_object('bricks', 'repo_info_v2.json').data.decode('utf-8')
        self.bricks = ast.literal_eval(_bricks)

        local_bricks_data = parse_local_bricks('plugins')

        for brick in self.bricks:
            for local_brick in local_bricks_data:
                if bformat(brick['name']) == bformat(local_brick['name']):
                    brick = brick.update(local_brick)
                    break

    def _remove_brique(self, name):
        """
        Remove a brick
        :param name: name of the brick
        """

        app = App(None, None)
        app.pluginManager.get_plugin('plugins.' + name).uninstall()
        app.remove_plugin('plugins.' + bformat(name))
        shutil.rmtree('plugins/' + name)

        try:
            data = Config().get("plugins")
            data.pop(name, None)
            Config().set("plugins", data)
        except KeyError:
            pass

        ActionLogger().info(self.strings.log_uninstalled.format(name))

    def action_briques(self):
        """
        Render the 'Brick' main page and get the plugin list online
        """

        # threading.Thread(target=self._update_briques).start()
        need_wizard = None
        need_wizard_name = None
        wizard_question = None
        brick_install_state = None
        brick_install_name = None
        self._update_briques()
        if len(self.config.get("need_wizard", [])) > 0:
            need_wizard = self.config.get("need_wizard")[0]
            app = App(None, None)
            wizard_question = app.pluginManager.get_plugin(need_wizard).get_next_wizard_question()
            need_wizard_name = need_wizard.split('.')[1]
        else:
            if len(self.config.get("brick_install_states_downloaded", [])) > 0:
                brick_install_state = "downloaded"
                brick_install_name = self.config.get("brick_install_states_downloaded", [])[0]
            elif len(self.config.get("brick_install_states_installed", [])) > 0:
                brick_install_state = "installed"
                brick_install_name = self.config.get("brick_install_states_installed", [])[0]
            elif len(self.config.get("brick_install_states_started", [])) > 0:
                brick_install_state = "started"
                brick_install_name = self.config.get("brick_install_states_started", [])[0]
                data = self.config.get("brick_install_states_started", [])
                data.pop(0)
                self.config.set("brick_install_states_started", data)
            elif len(self.config.get("brick_install_states_failed", [])) > 0:
                brick_install_state = "failed"
                brick_install_name = self.config.get("brick_install_states_failed", [])[0]
                data = self.config.get("brick_install_states_failed", [])
                data.pop(0)
                self.config.set("brick_install_states_failed", data)

        return self.render("briques", {
            "bricks": self.bricks,
            "rows": self.ROWS,
            "lines": self.LINES,
            "need_wizard": need_wizard is not None,
            "need_wizard_full_name": need_wizard,
            "need_wizard_name": need_wizard_name,
            "wizard_question": wizard_question,
            "need_install": brick_install_state is not None,
            "brick_install_state": brick_install_state,
            "brick_install_name": brick_install_name
        })

    def action_briques_process(self):
        """
        Wizard process
        """
        request.body.read()

        if 'plugin_wizard' in request.forms.keys():
            plugin = request.forms.plugin_wizard
            app = App(None, None)
            wizard_question = app.pluginManager.get_plugin(plugin).get_next_wizard_question()
            if wizard_question.response_type == Wizard.TYPE_BOOL:
                if 'response' in request.forms.keys():
                    value = True
                else:
                    value = False
            else:
                value = request.forms.response
            app.pluginManager.get_plugin(plugin).config.set(wizard_question.config_key, value)

            if app.pluginManager.get_plugin(plugin).get_next_wizard_question() is None:
                config_wizard = self.config.get("need_wizard")
                config_wizard.pop(0)
                self.config.set("need_wizard", config_wizard)

        return self.action_briques()

    def action_download(self, name):
        """
        Callback download a plugin
        """
        # threading.Thread(target=self._download_brique, args=(name,)).start()

        if len(self.config.get("brick_install_states_downloaded", [])) > 0:
            self.install_brick(self.config.get("brick_install_states_downloaded", [])[0])
            data = self.config.get("brick_install_states_downloaded", [])
            data.pop(0)
            self.config.set("brick_install_states_downloaded", data)
        elif len(self.config.get("brick_install_states_installed", [])) > 0:
            self.start_brick(self.config.get("brick_install_states_installed", [])[0])
            data = self.config.get("brick_install_states_installed", [])
            data.pop(0)
            self.config.set("brick_install_states_installed", data)
        else:
            self.download_brick(name)

        redirect("/internal_plugins/briques/")

    def action_remove(self, name):
        """
        Callback remove a plugin
        """
        # threading.Thread(target=self._remove_brique, args=(name,)).start()
        self._remove_brique(name)

        redirect("/internal_plugins/briques/")

    def download_brick(self, name):
        logging.debug("Download brick " + name)
        name = name.strip().lower()
        if len(self.bricks) == 0:
            self._update_briques()

        filtered_dict = filter(lambda x: bformat(x['name']) == name, self.bricks)

        brick = list(filtered_dict)[0]

        version = brick['latest_version']
        fname = '{0}_{1}.tar.gz'.format(name, version)
        fpath = 'plugins/{0}'.format(fname)

        client = Minio('minio.simtio.charly-hue.fr', access_key='', secret_key='', secure=True)
        client.fget_object('bricks', fname, fpath)

        with tarfile.open(fpath) as tar:
            tar.extractall('plugins/' + name)
            tar.close()
            os.remove(fpath)

        with open('plugins/' + name + '/.version', 'w') as vfile:
            vfile.write("VERSION={}\n".format(brick['latest_version']))
            vfile.write("LOGO={}\n".format(brick['logo']))
            vfile.write("NAME={}\n".format(brick['name']))
            vfile.close()

        self.save_install_sate(name, 'downloaded')

    def install_brick(self, name):
        logging.debug("Install brick " + name)
        app = App()
        if PluginManager().has_plugin(name):
            app.remove_plugin('plugins.' + name)
            app.add_plugin('plugins.' + name, True)
        else:
            app.add_plugin('plugins.' + name, False)
        if app.pluginManager.get_plugin('plugins.' + name).has_wizard():
            need_wizard = self.config.get('need_wizard', [])
            need_wizard.append('plugins.' + name)
            self.config.set('need_wizard', need_wizard)

        self.save_install_sate(name, 'installed')

    def start_brick(self, name):
        logging.debug("Start brick " + name)
        try:
            App().pluginManager.get_plugin('plugins.' + name).install()
            self.save_install_sate(name, 'started')
            ActionLogger().info(self.strings.log_installed.format(name))
        except Exception as e:  # noqa: E722
            self._remove_brique(name)
            self.save_install_sate(name, 'failed')
            logging.error(e)

    def save_install_sate(self, name, state):
        states = self.config.get('brick_install_states_' + state, [])
        if len(states) >= 1 and states[0] == name:
            states.pop(0)
        else:
            states.append(name)
        self.config.set('brick_install_states_' + state, states)
