import os


def read_version_file(path):
    """
    Cette fonction permet de lire le numéro de version dans un fichier
    :param path: Chemin vers le fichier
    :return: Le numéro de version
    """
    with open(path, 'r') as vfile:
        data = vfile.read()
        li = data.split('\n')
        return {'current_version': li[0].split('=')[1],
                'logo': li[1].split('=')[1]}


def parse_local_bricks(path):
    """
    Cette fonction retourne la liste des briques installées
    :param path: Le nom du dossier à parcourir
    :return: Un tableau contenant les briques locales
    """
    data = []
    for r, d, file in os.walk(path):
        if r != path:
            continue
        if d:
            for sd in d:
                f = "plugins/" + sd + "/.version"
                sub_dict = read_version_file(f)
                sub_dict.update({'name': sd})
                data.append(sub_dict)
    return data


def bformat(s):
    """
    Cette fonction retourne une chaine formatée
    :param s: Chaine brute
    :return: Chaine formatée
    """
    return s.strip().lower()
