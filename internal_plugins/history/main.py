from core import BasePlugin, Page, MenuItem, ActionLogger


class Plugin(BasePlugin):
    def __init__(self):
        super(Plugin, self).__init__()
        self.register_page(Page("", Page.METHOD_GET, self.action_index))
        self.register_page(Page("<page>", Page.METHOD_GET, self.action_index))
        self.register_menu_item(MenuItem(self.strings.History, "fas fa-history", "/internal_plugins/history/"))

    def action_index(self, page=1):
        return self.render("index", {"logs": ActionLogger().get_all(int(page) - 1),
                                     "nb_pages": ActionLogger().get_nb_pages(),
                                     "current_page": int(page)
                                     })
